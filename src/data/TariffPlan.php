<?php

namespace bil24api\data;

use bil24api\BaseObject;

class TariffPlan extends BaseObject
{
    /**
     * id тарифного плана
     *
     * @var int
     */
    public $tariffPlanId;

    /**
     * Название тарифного плана.
     *
     * @var string
     */
    public $tariffPlanName;
}
