<?php

namespace bil24api\requests;

use bil24api\BaseRequestObject;

class GetAllAction extends BaseRequestObject
{
    /**
     * @var int
     */
    public $cityId;

    /**
     * @var int
     */
    public $actionId;

    /**
     * @var int
     */
    public $userId;

    public static function getCommand()
    {
        return 'GET_ALL_ACTIONS';
    }

    public function getRequiredAttributes()
    {
        return array_merge(parent::getRequiredAttributes(), []);
    }
}
