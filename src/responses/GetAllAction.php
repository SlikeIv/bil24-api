<?php

namespace bil24api\responses;

use bil24api\BaseResponseObject;

class GetAllAction extends BaseResponseObject
{
    /**
     * представление.
     *
     * @var \bil24api\data\City[]
     */
     public $cityList;

     /**
      * представление.
      *
      * @var \bil24api\data\Kind[]
      */
     public $kindList;

      /**
       * представление.
       *
       * @var \bil24api\data\Action[]
       */
     public $actionList;
}
