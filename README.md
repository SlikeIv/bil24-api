## Библиотека для работы с билетной системой Bil24
https://bil24.pro/api.htm

### Список изменений:
* Изменен хост на api.bil24.pro (ранее был api.bil24.ru)
* Добавлены методы getAllAction и getVenueTypes
* Добавлен новый тип TariffPlan. 
* Помечено поле legalOwner класса Action как "deprecated", вместо него используются три новых поля: legalOwnerName, legalOwnerInn, legalOwnerPhone
* Добавлено поля phoneRequired и fullNameRequired в класс ActionEvent
* Добавлено полe tariffIdMap в класс CategoryPrice

### Установка
* выполнить
```
git clone https://SlikeIv@bitbucket.org/SlikeIv/bil24-api.git
```
* добавить в composer.json в секцию "autoload" -> "psr-4" путь до папки с файлами библиотеки, например:
```
"bil24api\\" : "app/bil24api/src/"
```
* выполнить 
```
composer dump-autoload --optimize
```

### Использование

```php


use bil24api\Client;

$client = new Client([
    'fid' => 1111, // ваш frontend id в системе bil24
    'token' => '23dfd343dfdd34dfd', // ваш токен в системе bil24
    // если есть данные пользователя, для использования методов, требующих авторизации
    'userId' => 111,
    'sessionId' => '23dfd343dfdd34dfd',
]);

//============== command  GET_ALL_ACTIONS =================   
$actions = $client->getAllAction();
    echo "<PRE>";
    print_r($actions);
    echo "</PRE>";
    
//============== command  GET_VENUE_TYPES =================   
$venueTypes = $client->getVenueTypes();
echo "<PRE>";
print_r($venueTypes);
echo "</PRE>";   


//============== command  GET_CITIES =================  
$moscowId = null;
$cities = $client->getCities();
foreach ($cities->cityList as $city) {
    if ($city->cityName == 'Москва') {
        $moscowId = $city->cityId;
    }
}

//============== command  GET_VENUES =================  
$venues = $client->getVenueList($moscowId);

foreach ($venues->venueList as $venue) {
	// ...
}
```